#!/usr/bin/python

#gssmcompress
# Kacey Coley
# February 9, 2014
# Tool for people who are too lazy to use the flags to compress various directories.
# Currently supports zip, tar, tar.gz and tgz
# Port from dpacompress
# Syntax: gssmcompress file.ext [uncompress directory]

import os
import os.path
import sys
import shutil
import subprocess

def compressArchive(archive, archiveBase, extension, archiveDir):
	print extension
	if extension == ".zip":
		try:
			print "Zipping " + archive + " to " 
			print archiveDir
			print "zip -r " + archiveBase + extension + " " + archive
			subprocess.call(["zip", "-r", archiveBase + extension, archive])
		except:
			print "Could not zip archive " + archive
	elif extension == ".tar":
		try:
			print "Taring..." + archive + " to "
			print archiveDir
			print "tar zcvf " + archiveBase + extension + " " + archive
			subprocess.call(["tar", "zcvf", archiveBase + extension, archive ])
		except:
			print "Could not tar archive " + archive
	elif extension == ".tar.gz" or extension == ".tgz":
		try:
			print "Targzing..." + archive + " to "
			print archiveDir
			print "archiveBase = " + archiveBase
			print "extension = " + extension
			print "tar cvzf " + archiveBase + extension + " " + archive
			subprocess.call(["tar", "cvzf",archiveBase  + extension,  archive])
		except:
			print "Could not tar.gz file " + archive
	elif extension == ".rar":
			print "Can not rar file " + archive
	elif extension == ".7z":
		try:
			print "7z-ing " + archive + " to " 
			print archiveDir
			print "7z a " + archiveBase + extension + " " + archive
			subprocess.call(["7z", "a", archiveBase + extension, archive])
		except:
			print "Could not 7z file " + archive
	else:
		print "Cannot archive " + archive






def help():
	print "GSSM Compress"
	print "gssmcompress: <directory> <directory.ext>"
	print "ext = zip, tar, tgz, tar.gz, 7z"
	exit(0)
def main():
	if len(sys.argv) < 3:
		print help()
		exit(0)
	if sys.argv[1] == '-h':
		print help()	
	if len(sys.argv) < 5:
		archiveBase = "" 
		archiveExtension = ""
		archiveDir = ""
		archive = sys.argv[1]
		archiveName = sys.argv[2]
		try:
			archiveBase,archiveExtension = os.path.splitext(archiveName)
			if archiveExtension in ['.gz']:
				archiveBase,archiveExtension2 = os.path.splitext(archiveBase)
				archiveExtension = archiveExtension2 + archiveExtension
			if len(sys.argv) == 3:
				archiveDir = os.path.abspath(sys.argv[2])
		except:
			print archiveDir
			print "Improper archive file"
			return
		# archive, archive extension, place to store archive
		compressArchive(archive, archiveBase, archiveExtension, archiveDir)
main()
