#!/usr/bin/python
# gssmInstallFedora20Packages.py
# Install packages for Fedora 20
# Created:August 11, 2014 - Kacey Coley
# Last Modified:March 25, 2015 - Kacey Coley
#
# Usage: ./gssmInstallFedora20Packages.py

import os
import sys
import shutil

def installOpenEXR():
    currentWorkingDirectory = os.getcwd()
    os.chdir('/tmp')
    os.system('git clone https://github.com/openexr/openexr.git')
    os.chdir('openexr')
    os.system('git reset --hard v2.2.0')
    os.chdir('IlmBase')
    os.system('./bootstrap')
    os.system('./configure --prefix=/usr/local')
    os.system('make')
    os.system('make install')
    try:
        os.environ["LD_LIBRARY_PATH"] += os.pathsep + "/usr/local/lib"
    except:
        os.environ["LD_LIBRARY_PATH"] = os.pathsep + "/usr/local/lib"

    try:
        os.environ["CPATH"] += os.pathsep + "/usr/local/include"
    except:
        os.environ["CPATH"] = os.pathsep + "/usr/local/include"
    os.chdir('../OpenEXR')
    os.system('./bootstrap')
    os.system('./configure --prefix=/usr/local')
    os.system('make -j8')
    os.system('make install')
    os.chdir('../../')
    os.system('rm -fr openexr')
    os.chdir(currentWorkingDirectory)

def installOIIO():
    currentWorkingDirectory = os.getcwd()
    os.chdir('/tmp')
    os.system('git clone https://github.com/OpenImageIO/oiio.git')
    os.chdir('oiio')
    os.system('git reset --hard Release-1.4.11')
    os.system('make INSTALLDIR="/usr/local" ILMBASE_HOME="/usr/local" OPENEXR_HOME="/usr/local" -j8 STOP_ON_WARNING=0')
    os.chdir('../')
    os.system('rm -fr oiio')
    os.chdir(currentWorkingDirectory)

def installGoogleChrome():
    file = open("/etc/yum.repos.d/google-chrome.repo", "w")
    file.write(
      "[google-chrome]\n\
name=google-chrome\n\
baseurl=http://dl.google.com/linux/chrome/rpm/stable/x86_64\n\
enabled=1\n\
gpgcheck=1\n\
gpgkey=https://dl-ssl.google.com/linux/linux_signing_key.pub")
    file.close()
    os.system('yum -y install google-chrome-stable')
# Packages we wish to install
packages = [
    'gcc-c++',
    'freeglut-devel',
    'glew-devel',
    'glfw-devel',
    'SDL2-devel',
    'valgrind',
    'blender',
    'boost-devel',
    'clang',
    'git',
    'PyQt4',
    'mercurial',
    'java-1.8.0-openjdk-devel',
    'tbb-devel',
    'doxygen',
    'cmake',
    'gimp',
    'inkscape',
    'zlib-devel',
    'libjpeg-turbo',
    'libjpeg-turbo-devel',
    'vim',
    'libpng-devel',
    'qt-devel',
    'libtiff-devel',
    'python-devel',
    'swig',
    'pygame',
    'fltk-devel',
    'freeglut-devel',
    'PyOpenGL',
    'autoconf',
    'automake',
    'libtool',
    'guake',
    'openssh',
    'p7zip',
    'htop',
    'cppunit',
    'gtest-devel',
    'ImageMagick-c++-devel',
    'audacity',
    'tiled',
    'openmpi-devel',
    'AntTweakBar-devel',
    'glm-devel',
    'SDL2-devel'
    ]


def installPackages():
    euid = os.geteuid()
    if euid != 0:
        print "Script not started as root.  Running sudo.."
        args = ['sudo', sys.executable] + sys.argv + [os.environ]
        os.execlpe('sudo', *args)

    # Command to install the packages
    installCommand = "yum -y install "
    os.system('yum -y update')

    for package in packages:
        os.system(installCommand + package)

    # enable ssh
    os.system('chkconfig sshd on')

    os.system('service sshd start')
    
    os.system('systemctl enable sshd.service')
    # installing atom
#    os.system('rpm -i ../atom.x86_64.rpm')

    # installing djv
#    os.system('rpm -i ../djv-1.0.3-Linux-64.rpm')

    # auto start guake
#    shutil.copy2('/usr/share/applications/guake.desktop', '/etc/xdg/autostart/')

    # install chrome
#    installGoogleChrome()

    #install OpenEXR
#    installOpenEXR()

    #install OpenImageIO
#    installOIIO()

    #install Sphinx
#    os.system('easy_install -U Sphinx')

if __name__ == "__main__":
    installPackages()
